from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipts}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    receipts = receipts.values("category__name").annotate(
        count=Count("category__name")
    )
    context = {"receipt_list": receipts}
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    accounts = Receipt.objects.filter(purchaser=request.user)
    accounts = accounts.values("account__name", "account__number").annotate(
        count=Count("account__name")
    )
    context = {"account_list": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "receipts/create_account.html", context)
